import os
import sys
from typing import Any
import json

import boto3

from fabrique_nodes_core import (
    BaseNode,
    NodesGroup,
    # Port,
    NodeData,
    UIParams,
    # UIPortGroupParams,
    # port_type,
    Array,
    NodeConfig,
    EmptyValue,
    root_port,
    is_valid_port,
    DestructurerUIParams,
    StructurerUIParams,
    Crypto
)
from fabrique_nodes_core.convertors import upd_jspath_output

from pydantic import BaseModel, Field
import jmespath

cur_file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f"{cur_file_dir}/..")

# https://perandrestromhaug.com/posts/writing-an-sqs-consumer-in-python/


class UIConfig(BaseModel):
    region_name: str = Field("us-east-1", title="Region name", description="AWS region name")
    queue_name: str = Field("demo", title="Queue name", description="SQS queue name")
    access_key_id: str = Field("", title="AWS access key id", description="AWS SQS access key id")
    secret_access_key: str = Field("", title="AWS Secret Access Key",
                                   description="AWS Secret Access Key",
                                   node_updater_callback_name="make_hashed_outputs_cb",
                                   # code_validator_callback_name="is_valid_code_cb",
                                   )
    access_key_hash: str = Field("",
                                 title="Secret Access Key Hash",
                                 description="AWS Secret Access Key Hash",
                                 visible=False
                                 )


class SQSBase(BaseNode):
    def __init__(self, cfg: NodeConfig, user_id: str):
        super().__init__(cfg, user_id)
        ui_config = UIConfig(**cfg.ui_config)
        crypto = Crypto(user_id)

        session = boto3.Session(
            aws_access_key_id=ui_config.access_key_id,
            aws_secret_access_key=crypto.decrypt(ui_config.access_key_hash),
            region_name=ui_config.region_name
        )

        # create an SQS client using the session
        # sqs = session.client('sqs')
        sqs = session.resource('sqs')
        self.queue = sqs.get_queue_by_name(QueueName=ui_config.queue_name)

    @classmethod
    def make_hashed_outputs_cb(cls, data: NodeData, user_id: str) -> NodeData:
        if data.ui_config['secret_access_key'] == '':
            return data  # do nothing
        crypto = Crypto(user_id)
        ui_config = UIConfig(**data.ui_config)
        data.ui_config['access_key_hash'] = crypto.encrypt(ui_config.secret_access_key)
        data.ui_config['secret_access_key'] = ''
        return data


class SQSWrite(SQSBase):
    """"""
    type_ = "SQSWrite"
    category = "IO"

    initial_config = NodeData(
        name="SQSWrite",
        g_ports_in=[
            [root_port],
        ],
        description="",
        ui_config=UIConfig(),
    )
    ui_params = UIParams(
        input_groups=[
            StructurerUIParams().input_groups[0],
        ],
        ui_config_schema=UIConfig.schema_json(),
    )

    def process(self, *inputs) -> str:
        """Create json string

        :return: json string
        :rtype: str
        """
        output = {}
        for i, p in enumerate([p for p in self.cfg.ports_in if p.visible]):
            if not p.code:
                output = {**output, **inputs[i]}
                continue
            upd_jspath_output(output, p.code, inputs[i])

        return [output]

    def process_universal(self, *array_args) -> list:
        nonnull_args = [a for a in array_args if a]
        if nonnull_args and isinstance(nonnull_args[0], Array):
            results_list = [self.process_universal(*args) for args in zip(*array_args) if self.is_valid_args(args)]
            return [value for values_lst in zip(*results_list) for value in values_lst]
        else:
            return self.process(*array_args)

    def process_many(self, microbatch) -> list:
        print('process_many SQSWrite microbatch', microbatch)
        values_lst = [self.process_universal(*args) for args in zip(*microbatch) if self.is_valid_args(args)]

        messages = [val for val_array in values_lst if val_array for val in val_array]

        for mes in messages:
            response = self.queue.send_message(
                MessageBody=json.dumps(mes)
            )
            assert response['ResponseMetadata']['HTTPStatusCode'] == 200
            print(f'> SQS {mes}')


class SQSRead(SQSBase):
    """"""

    type_ = "SQSRead"
    category = "IO"
    initial_config = NodeData(
        name="SQSRead",
        g_ports_out=[
            [root_port],
            [is_valid_port],
        ],
        description="",
        ui_config=UIConfig(),
    )
    ui_params = UIParams(
        output_groups=DestructurerUIParams().output_groups,
        ui_config_schema=UIConfig.schema_json(),
    )

    def __init__(self, cfg: NodeConfig, user_id: str):
        super().__init__(cfg, user_id)
        self.is_emulation_mode = True  # postinit fixed in engine, TODO add in init

    def process(self, input_json) -> list[Any]:
        """Create list of velues from json element

        :return: list constants
        :rtype: list[Any]
        """

        has_is_valid = self.cfg.g_ports_out[1][0].visible
        is_valid = True

        invalid_outputs = [EmptyValue() for p in self.cfg.g_ports_out[0] if p.visible]

        try:
            input_dict = json.loads(input_json)
        except Exception:  # if invalid json
            if has_is_valid:
                invalid_outputs.append(False)
            return invalid_outputs

        outputs = []
        for p in self.cfg.g_ports_out[0]:
            if not p.visible:
                continue

            if p.code == "":
                outputs.append(input_dict)
                continue

            output = jmespath.search(p.code, input_dict)

            if p.required and output is None:
                is_valid = False
                outputs = invalid_outputs
                break

            outputs.append(output)

        if has_is_valid:
            outputs.append(is_valid)
        return outputs

    def _receive_all(self, mes_lst: list = None, MaxNumberOfMessages=10, WaitTimeSeconds=1) -> list:
        mes_lst = [] if not mes_lst else mes_lst
        messages = self.queue.receive_messages(
            MaxNumberOfMessages=MaxNumberOfMessages,
            WaitTimeSeconds=WaitTimeSeconds
        )
        if not messages:
            return mes_lst
        if len(mes_lst) > MaxNumberOfMessages:
            return mes_lst
        mes_lst += messages
        return self._receive_all(mes_lst, MaxNumberOfMessages, WaitTimeSeconds)

    def process_many(self, _microbatch) -> list:
        if self.is_emulation_mode:
            num_of_messages = 10
            timeout = 1
            messages = self._receive_all(None, num_of_messages, timeout)
        else:
            num_of_messages = 10000
            timeout = 0.2
            messages = self.queue.receive_messages(
                MaxNumberOfMessages=num_of_messages,
                WaitTimeSeconds=timeout
            )

        results = []
        for message in messages:
            outputs = self.process(message.body)
            message.delete()
            results.append(outputs)

        print(f'< SQS {results}')
        return results


class Node(NodesGroup):
    """Class that contains varieties of nodes in class attribute `nodes_array`"""

    type_ = "PubSub"
    nodes_array = [SQSWrite, SQSRead]
