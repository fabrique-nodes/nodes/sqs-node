import pytest
from fabrique_nodes_core.validators import validate_node
from fabrique_nodes_core.convertors import convert_node_data_to_config
from fabrique_nodes_core import NodeData, root_port, is_valid_port
from fabrique_nodes_core.db.fake_db import FakeDB
from unittest.mock import patch

import os
import sys

from dotenv import load_dotenv
load_dotenv()

cur_file_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f"{cur_file_dir}/..")


from src.node import Node, SQSRead, SQSWrite, UIConfig
from src.node import NodesGroup


assert issubclass(Node, NodesGroup)

node_classes = {n.type_: n for n in Node.nodes_array}
assert SQSRead == node_classes["SQSRead"]
assert SQSWrite == node_classes["SQSWrite"]


@pytest.mark.parametrize("node", (SQSRead, SQSWrite))
def test_positive_validations(node):
    validate_node(node)


@patch('fabrique_nodes_core.crypto.DB', new=FakeDB)
def test_crypto():
    # user_id берется из самосотоятельного обращения апишки к wix, потому хакнуть крайне сложно
    user_id = '_user_id_'

    ui_config = dict(
        region_name="us-east-1",
        queue_name="test",
        access_key_id='',
        secret_access_key="SECRET_ACCESS_KEY"
    )

    node_data = NodeData(
        name="",
        g_ports_out=[[root_port]],
        ui_config=ui_config
    )

    updated_node_data = SQSRead.make_hashed_outputs_cb(node_data, user_id)

    # убедиться, что стёрся секрет
    ui_cfg = UIConfig(**updated_node_data.ui_config)
    enc_key = ui_cfg.access_key_hash
    assert ui_cfg.secret_access_key == ''
    assert len(enc_key)

    # убедиться, что с пустым секретом она ничего не перезапишет при повторном вызове
    upd_updated_node_data = SQSRead.make_hashed_outputs_cb(updated_node_data, user_id)
    ui_cfg = UIConfig(**upd_updated_node_data.ui_config)
    assert ui_cfg.access_key_hash == enc_key


@patch('fabrique_nodes_core.crypto.DB', new=FakeDB)
def test_general():
    SQS_ACCESS_KEY_ID = os.environ['SQS_ACCESS_KEY_ID']
    SQS_SECRET_ACCESS_KEY = os.environ['SQS_SECRET_ACCESS_KEY']

    user_id = '_user_id_'

    ui_config = dict(
        region_name="us-east-1",
        queue_name="test",
        access_key_id=SQS_ACCESS_KEY_ID,
        secret_access_key=SQS_SECRET_ACCESS_KEY
    )

    node_data_read = NodeData(
        name="",
        g_ports_out=[[root_port], [is_valid_port]],
        ui_config=ui_config.copy()
    )

    node_data_write = NodeData(
        name="",
        g_ports_in=[[root_port], [is_valid_port]],
        ui_config=ui_config.copy()
    )

    # cfg_read = convert_node_data_to_config(node_data_read)
    # cfg_write = convert_node_data_to_config(node_data_write)

    upd_node_data_read = SQSRead.make_hashed_outputs_cb(node_data_read, user_id)
    upd_node_data_write = SQSRead.make_hashed_outputs_cb(node_data_write, user_id)

    sqs_read = SQSRead(convert_node_data_to_config(upd_node_data_read), user_id)
    sqs_write = SQSWrite(convert_node_data_to_config(upd_node_data_write), user_id)
    # inputs = node.process(*input_val)
    test_values = [[{'key': 'value0'}, {'key': 'value1'}, {'key': 'value2'}]]
    sqs_write.process_many(test_values)
    result = sqs_read.process_many([[]])
    assert len(result) == len(test_values[0])
    assert all([r[0] in test_values[0] for r in result])

    # assert json.dumps(inputs) == json.dumps(expected_inputs)
